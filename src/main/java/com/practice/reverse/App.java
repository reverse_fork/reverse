package com.practice.reverse;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        System.out.println("Practice Problem Reverse");
        System.out.println(reverseSentence("test and another with spaces"));
    }
    
    public static String reverseSentence(String sentence)
    {
    	String output = "";
    	
    	if(sentence == null)
    		output = "";
    	else
    	{    		
    		StringBuilder sb = new StringBuilder();
    		for(int i = sentence.length(); i > 0; i--)
    		{
    			sb.append(sentence.charAt(i-1));    			
    		}
    		output = sb.toString();   		
    	}
    	
    	return output;
    }
    public static String reverseEveryWord(String sentence)
    {
    	String output = "";
    	
    	return output;
    }
}
